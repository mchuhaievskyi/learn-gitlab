package io.mchuhaievskyi.learn.gitlab;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExampleClassTest
{

    @Before
    public void setUp() throws Exception
    {
        System.out.println("Hello from setUp");
    }

    @After
    public void tearDown() throws Exception
    {
        System.out.println("Hello from tearDown");
    }

    @Test
    public void exampleMethod()
    {
        final ExampleClass exampleClass = new ExampleClass();

        assertNull(exampleClass.exampleMethod());
    }
}